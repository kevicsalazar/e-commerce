package com.kevicsalazar.ecommerce.client.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kevicsalazar.ecommerce.client.App;
import com.kevicsalazar.ecommerce.client.AppComponent;
import com.kevicsalazar.ecommerce.client.base.interfaces.BasePresenter;

import butterknife.ButterKnife;

/**
 * Created by Kevin Salazar
 */
public abstract class BaseFragment extends Fragment {

    protected Context CONTEXT;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        CONTEXT = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getFragmentLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
        injectDependencies();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(getPresenter() != null) getPresenter().onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(getPresenter() != null) getPresenter().onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbindViews();
    }

    /**
     * Specify the layout of the fragment to be inflated in the {@link BaseFragment#onCreateView(LayoutInflater, ViewGroup, Bundle)}
     */
    protected abstract int getFragmentLayout();

    /**
     * @return The presenter attached to the fragment. This must extends from {@link BasePresenter}
     */
    protected abstract BasePresenter getPresenter();

    private void injectDependencies() {
        setUpComponent(((App) getActivity().getApplication()).getComponent());
    }

    /**
     * Replace all the annotated fields with ButterKnife annotations with the proper value
     */
    private void bindViews(View rootView) {
        ButterKnife.bind(this, rootView);
    }

    private void unbindViews() {
        ButterKnife.unbind(this);
    }

    /**
     * This method will setup the injector and will commit the dependencies injections.
     */
    protected abstract void setUpComponent(AppComponent component);

}
