package com.kevicsalazar.ecommerce.client

import android.content.Context
import com.kevicsalazar.ecommerce.client.base.annotation.PerApp
import com.kevicsalazar.ecommerce.client.base.annotation.PerAppOld
import com.kevicsalazar.ecommerce.client.modules.NetworkService

import dagger.Module
import dagger.Provides

/**
 * @author Kevin Salazar
 * *
 * @link kevicsalazar.com
 */
@Module
class AppModule(private val app: BaseApp) {

    @Provides @PerApp fun provideContext(): Context {
        return app
    }

    /*@Provides @PerApp fun provideResourcesProvider(): ResourceProvider {
        return ResourceProvider(app as App)
    }

    @Provides @PerApp fun providePreferencesProvider(): PreferenceProvider {
        return PreferenceProvider(app as App)
    }

    @Provides @PerApp fun provideAnalyticsProvider(): AnalyticsProvider {
        return AnalyticsProvider(app as App)
    }

    @Provides @PerApp fun provideNetworkService(): NetworkService {
        return NetworkService(app as App)
    }*/

    @Provides @PerApp fun provideNetworkService(): NetworkService {
        return NetworkService(app as App)
    }

}
