package com.kevicsalazar.ecommerce.client.ui.adapters;

import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kevicsalazar.ecommerce.client.R;
import com.kevicsalazar.ecommerce.client.ui.mvp.model.Locality;
import com.kevicsalazar.ecommerce.client.utils.Tag;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Kevin Salazar
 */
public class SimpleRecyclerAdapter extends RecyclerView.Adapter<SimpleRecyclerAdapter.ViewHolder> {

    private List<Pair<Integer, Object>> list;

    public SimpleRecyclerAdapter() {
        list = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).first;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutId;
        switch (viewType) {
            case Tag.TEXT: { layoutId = R.layout.item_text; break; }
            default: return null;
        }
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Object item = list.get(position).second;
        switch (getItemViewType(position)) {
            case Tag.TEXT:
                Locality locality = (Locality) item;
                holder.tvTitle.setText(locality.getName());
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addAll(List<?> newList) {
        for (Object o : newList) {
            if (o instanceof Locality) {
                list.add(new Pair<>(Tag.TEXT, o));
            }
        }
        notifyDataSetChanged();
    }

    public void clear() {
        if (!list.isEmpty()) {
            list.clear();
            notifyDataSetChanged();
        }
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tvTitle)
        TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
