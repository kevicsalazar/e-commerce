package com.kevicsalazar.ecommerce.client

import android.content.Context
import com.kevicsalazar.ecommerce.client.base.annotation.PerApp

import com.kevicsalazar.ecommerce.client.base.annotation.PerAppOld
import com.kevicsalazar.ecommerce.client.modules.AnalyticsProvider
import com.kevicsalazar.ecommerce.client.modules.NetworkService
import com.kevicsalazar.ecommerce.client.modules.PreferenceProvider
import com.kevicsalazar.ecommerce.client.modules.ResourceProvider
import com.kevicsalazar.ecommerce.client.ui.mvp.views.MainActivity

import dagger.Component

/**
 * @author Kevin Salazar
 * *
 * @link kevicsalazar.com
 */
@PerApp
@Component(modules = arrayOf(AppModule::class))
public interface AppComponent {

    fun inject(app: App)

    fun inject(activity: MainActivity)

    fun context(): Context

    fun networkService(): NetworkService

}