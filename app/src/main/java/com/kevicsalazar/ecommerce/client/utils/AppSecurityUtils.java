package com.kevicsalazar.ecommerce.client.utils;

import java.security.MessageDigest;

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
public class AppSecurityUtils {

    public static String convertToMd5(final String md5){
        StringBuffer sb;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes("UTF-8"));
            sb = new StringBuffer();
            for (byte anArray : array) {
                sb.append(Integer.toHexString((anArray & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
