package com.kevicsalazar.ecommerce.client.utils;

import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
public class AppParseUtils<T extends ParseObject> {

    public List<T> convertToParseModel(List<ParseObject> list){
        List<T> newList = new ArrayList<>();
        for (ParseObject parseObject : list){
            newList.add((T)parseObject);
        }
        return newList;
    }

}
