package com.kevicsalazar.ecommerce.client;

import android.support.multidex.MultiDexApplication;

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
public abstract class BaseApp extends MultiDexApplication {

    protected AppComponent initDaggerComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

}
