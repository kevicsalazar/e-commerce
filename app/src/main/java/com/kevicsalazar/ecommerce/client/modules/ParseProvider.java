package com.kevicsalazar.ecommerce.client.modules;

import com.kevicsalazar.ecommerce.client.R;
import com.kevicsalazar.ecommerce.client.base.enums.LoadStatus;
import com.kevicsalazar.ecommerce.client.base.enums.UpdateTime;
import com.kevicsalazar.ecommerce.client.base.interfaces.LoadCallback;
import com.kevicsalazar.ecommerce.client.base.utils.Clock;
import com.kevicsalazar.ecommerce.client.base.utils.ParserUtils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by Kevin Salazar
 */

@SuppressWarnings("unchecked")
public class ParseProvider {

    private LoadCallback cb;
    private PreferenceProvider pref;
    private NetworkService net;

    public ParseProvider(LoadCallback cb, PreferenceProvider pref, NetworkService net) {
        this.cb = cb;
        this.pref = pref;
        this.net = net;
    }

    public <C extends ParseObject> void findSomething(Class<C> c) {
        cb.onLoadStatus(LoadStatus.LOADING);
        ParseQuery<C> query = ParseQuery.getQuery(c);
        query.findInBackground((list, e1) -> {
            cb.onLoadStatus(LoadStatus.LOADED);
            if (e1 != null) { cb.onLoadFailure(getErrorMessage(e1)); return; }
            cb.onLoadSuccess(ParserUtils.parse(list, c));
        });
    }

    public <C extends ParseObject> void findSomething(Class<C> c, UpdateTime time, String tag) {
        cb.onLoadStatus(LoadStatus.LOADING);
        ParseQuery<C> query = ParseQuery.getQuery(c);
        if (!UpdateTime.isTimeToUpdate(pref.getLong(tag, 0), time)) {
            query.fromLocalDatastore().findInBackground((list, e1) -> {
                cb.onLoadStatus(LoadStatus.LOADED);
                if (e1 != null) { return; }
                cb.onLoadSuccess(list);
            });
            return;
        }
        if (net.isOnline()) {
            query.findInBackground((newList, e1) -> {
                cb.onLoadStatus(LoadStatus.LOADED);
                if (e1 != null) { cb.onLoadFailure(getErrorMessage(e1)); return; }
                query.fromLocalDatastore().findInBackground((localList, e2) -> {
                    if (e2 != null) { cb.onLoadFailure(getErrorMessage(e2)); return; }
                    ParseObject.unpinAllInBackground(localList, e3 -> {
                        if (e3 != null) { cb.onLoadFailure(getErrorMessage(e3)); return; }
                        ParseObject.pinAllInBackground(newList, e4 -> {
                            if (e4 != null) { cb.onLoadFailure(getErrorMessage(e4)); return; }
                            pref.putLong(tag, Clock.REAL.millis());
                            cb.onLoadSuccess(newList);
                        });
                    });
                });
            });
        } else {
            cb.onLoadStatus(LoadStatus.LOADED);
            cb.onLoadFailure(R.string.error_network);
        }

    }

    private int getErrorMessage(ParseException e) {
        e.printStackTrace();
        switch (e.getCode()) {
            case ParseException.CONNECTION_FAILED:
                return R.string.error_network;
            default:
                return R.string.error_generic;
        }
    }

}
