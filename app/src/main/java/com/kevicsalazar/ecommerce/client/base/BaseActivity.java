package com.kevicsalazar.ecommerce.client.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.kevicsalazar.ecommerce.client.App;
import com.kevicsalazar.ecommerce.client.AppComponent;
import com.kevicsalazar.ecommerce.client.base.interfaces.BasePresenter;

import butterknife.ButterKnife;

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
public abstract class BaseActivity extends AppCompatActivity {

    /**
     * The onCreate base will set the view specified in {@link #getLayout()} and will
     * inject dependencies and views.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        injectDependencies();
        injectViews();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(getPresenter() != null) getPresenter().onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(getPresenter() != null) getPresenter().onStop();
    }

    /**
     * @return The layout that's gonna be the activity view.
     */
    protected abstract int getLayout();

    /**
     * @return The presenter attached to the activity. This must extends from {@link BasePresenter}
     */
    protected abstract BasePresenter getPresenter();

    /**
     * Setup the object graph and inject the dependencies needed on this activity.
     */
    private void injectDependencies() {
        setUpComponent(((App) getApplication()).getComponent());
    }

    /**
     * Every object annotated with {@link butterknife.Bind} its gonna injected trough butterknife
     */
    private void injectViews() {
        ButterKnife.bind(this);
    }

    public abstract void setUpComponent(AppComponent appComponent);

}
