package com.kevicsalazar.ecommerce.client.ui.mvp.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by Kevin Salazar
 */
@ParseClassName(ParseModel.LOCALITY)
public class Locality extends ParseObject {

    public String getName() {
        return getString("name");
    }

    public static ParseQuery<Locality> getQuery() {
        return ParseQuery.getQuery(Locality.class);
    }

}