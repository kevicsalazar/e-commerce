package com.kevicsalazar.ecommerce.client.ui.mvp.model;

import com.parse.ParseObject;

/**
 * Created by Kevin Salazar
 */
public class ParseModel {

    public final static String LOCALITY = "Locality";

    public static void register() {
        ParseObject.registerSubclass(Locality.class);
    }

}
