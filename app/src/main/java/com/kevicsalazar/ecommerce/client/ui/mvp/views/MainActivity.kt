package com.kevicsalazar.ecommerce.client.ui.mvp.views

import android.os.Bundle
import android.util.Log
import android.widget.Toast

import com.kevicsalazar.ecommerce.client.AppComponent
import com.kevicsalazar.ecommerce.client.R
import com.kevicsalazar.ecommerce.client.base.BaseActivity
import com.kevicsalazar.ecommerce.client.base.enums.LoadStatus
import com.kevicsalazar.ecommerce.client.base.interfaces.BasePresenter
import com.kevicsalazar.ecommerce.client.base.interfaces.LoadCallback
import com.kevicsalazar.ecommerce.client.ui.mvp.model.Locality
import kotlinx.android.synthetic.activity_demo.*

class MainActivity : BaseActivity(), LoadCallback<List<Locality>> {

    override fun getLayout(): Int {
        return R.layout.activity_demo
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tvTitle.text = "hola"
    }

    override fun getPresenter(): BasePresenter? {
        return null
    }

    override fun setUpComponent(appComponent: AppComponent) {
        appComponent.inject(this);
    }

    override fun onLoadStatus(status: LoadStatus) {
        Log.e("LoadStatus", status.toString())
    }

    override fun onLoadSuccess(list: List<Locality>) {

    }

    override fun onLoadFailure(idMessage: Int) {
        Toast.makeText(this, idMessage, Toast.LENGTH_SHORT).show()
    }

}
