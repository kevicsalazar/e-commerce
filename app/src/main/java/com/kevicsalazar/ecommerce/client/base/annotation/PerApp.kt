package com.kevicsalazar.ecommerce.client.base.annotation

import kotlin.annotation.Retention
import kotlin.annotation.AnnotationRetention.RUNTIME
import javax.inject.Qualifier
import javax.inject.Scope

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
@Scope
@Qualifier
@Retention(RUNTIME)
annotation public class PerApp