package com.kevicsalazar.ecommerce.client.base.annotation;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Kevin Salazar
 */
@Scope
@Retention(RUNTIME)
public @interface PerActivityOld {
}
