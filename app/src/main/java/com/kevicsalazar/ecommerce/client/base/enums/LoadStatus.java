package com.kevicsalazar.ecommerce.client.base.enums;

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
public enum LoadStatus {

    LOADING("loading"),
    LOADED("loaded");

    private String status;

    LoadStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}
