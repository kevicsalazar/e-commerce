package com.kevicsalazar.ecommerce.client.base.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kevin Salazar
 */

@SuppressWarnings("all")
public class ParserUtils {

    public static <T, C> List<C> parse(List<T> pList, Class<C> c) {
        List<C> newList = new ArrayList<>();
        for (T o : pList) {
            newList.add((C) o);
        }
        return newList;
    }

}
