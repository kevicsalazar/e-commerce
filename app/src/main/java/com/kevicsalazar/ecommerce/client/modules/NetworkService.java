package com.kevicsalazar.ecommerce.client.modules;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.kevicsalazar.ecommerce.client.App;


/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
public class NetworkService {

    private App app;

    public NetworkService(App app) {
        this.app = app;
    }

    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) app.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

}
