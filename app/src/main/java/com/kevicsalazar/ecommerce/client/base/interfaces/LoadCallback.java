package com.kevicsalazar.ecommerce.client.base.interfaces;

import com.kevicsalazar.ecommerce.client.base.enums.LoadStatus;

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
public interface LoadCallback<L> {

    /**
     * Called when load status changes
     *
     * @param status can be
     *               {@link LoadStatus#LOADED}
     *               {@link LoadStatus#LOADING}
     */
    void onLoadStatus(LoadStatus status);

    /**
     * Called when load finished successfully
     *
     * @param l the generic type defined in angle brackets
     */
    void onLoadSuccess(L l);

    /**
     * Called when load finished unsuccessfully
     *
     * @param idMessage the string resId for message
     */
    void onLoadFailure(int idMessage);

}
