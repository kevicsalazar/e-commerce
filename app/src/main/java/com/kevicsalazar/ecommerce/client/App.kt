package com.kevicsalazar.ecommerce.client

import com.kevicsalazar.ecommerce.client.ui.mvp.model.ParseModel
import com.parse.Parse

class App : BaseApp() {

    private var component: AppComponent? = null

    override fun onCreate() {
        super.onCreate()
        setupGraph()
        ParseModel.register()
        Parse.enableLocalDatastore(this)
        Parse.initialize(this, BuildConfig.PARSE_APPLICATION_ID, BuildConfig.PARSE_CLIENT_KEY)
    }

    private fun setupGraph() {
        component = initDaggerComponent()
        getComponent().inject(this)
    }

    public fun getComponent(): AppComponent {
        return component!!
    }

}
