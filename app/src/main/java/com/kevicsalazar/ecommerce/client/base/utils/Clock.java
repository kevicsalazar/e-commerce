package com.kevicsalazar.ecommerce.client.base.utils;

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
public interface Clock {

    long millis();

    long nanos();

    Clock REAL = new Clock() {
        @Override
        public long millis() {
            return System.currentTimeMillis();
        }

        @Override
        public long nanos() {
            return System.nanoTime();
        }
    };

}
