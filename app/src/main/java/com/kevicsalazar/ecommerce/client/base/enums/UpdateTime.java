package com.kevicsalazar.ecommerce.client.base.enums;

import com.kevicsalazar.ecommerce.client.utils.AppDateUtils;

import java.util.Date;

/**
 * Created by Kevin Salazar
 */
public enum UpdateTime {

    NOW(0),                 // Ahora
    MIN(10),                // 10 segundos
    AUTOHIGH(60 * 30),      // 30 minutos
    AUTOMED(60 * 60 * 6),   //  6 horas
    AUTOLOW(60 * 60 * 12);  // 12 horas

    private int time;

    UpdateTime(int time) {
        this.time = time;
    }

    public int getTime() {
        return time;
    }

    public static boolean isTimeToUpdate(long lasUpdatedTime, UpdateTime time) {
        Date lastUpdated = null;
        if (lasUpdatedTime != 0) {
            lastUpdated = new Date(lasUpdatedTime);
        }
        return lastUpdated == null || AppDateUtils.addSeconds(lastUpdated, time.getTime()).getTime() < new Date().getTime();
    }

}
